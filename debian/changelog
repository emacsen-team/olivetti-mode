olivetti-mode (2.0.7-1) unstable; urgency=medium

  * Manually download & merge antisocial & noncollaborative dgit upload.
  * New upstream release.
  * Import updated upstream copyright statement and Upstream-Contact.
  * Declare Standards-Version 4.7.0 (no changes required).

 -- Nicholas D Steeves <sten@debian.org>  Mon, 04 Nov 2024 13:54:19 -0500

olivetti-mode (2.0.5-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 23:01:26 +0900

olivetti-mode (2.0.5-1) unstable; urgency=medium

  * New upstream release.
  * Rebase patch onto this release.
  * Drop version qualifier for emacs Recommends, because even oldoldstable has
    the required version.
  * Update upstream copyright years as well as my years.
  * Update Upstream-Contact.
  * No longer specify "NEWS" in d/docs, because upstream has migrated it to
    markdown, which is already handled by the "*.md" rule.
  * Declare Standards-Version 4.6.2 (no changes required).

 -- Nicholas D Steeves <sten@debian.org>  Sat, 18 Nov 2023 15:13:42 -0500

olivetti-mode (2.0.4-1) unstable; urgency=medium

  * New upstream release.
  * Rebase patch onto this release.

 -- Nicholas D Steeves <sten@debian.org>  Sun, 14 Nov 2021 12:03:38 -0500

olivetti-mode (2.0.3-1) unstable; urgency=medium

  * New upstream release.  Consult upstream NEWS file in /usr/share/doc
    for the list of changes.
  * Rebase 0001-Restore-established-keybindings.patch onto this release,
    and reject the new 'C-c |' binding for olivetti-set-width.
  * Update README.Debian.
  * Update upstream copyright years and upstream contact.
  * Update my copyright years.
  * Update debian/docs for new location of screenshot.

 -- Nicholas D Steeves <sten@debian.org>  Mon, 13 Sep 2021 17:41:39 -0400

olivetti-mode (1.11.3-1) unstable; urgency=medium

  * New upstream release.
  * Rebase patch queue onto this release.
  * Update upstream copyright years.
  * Update my email address.

 -- Nicholas D Steeves <sten@debian.org>  Tue, 09 Feb 2021 23:23:02 -0500

olivetti-mode (1.11.2-1) unstable; urgency=medium

  * New upstream release.
  * Rebase patch queue onto this release.
  * Install upstream NEWS file and screenshot used in README.md.
  * Ignore upstream Makefile by overriding dh_auto_build and dh_auto_install.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Tue, 03 Nov 2020 20:29:25 -0500

olivetti-mode (1.11.1-1) unstable; urgency=medium

  * New upstream release.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Sat, 04 Jul 2020 13:54:23 -0400

olivetti-mode (1.11.0-1) unstable; urgency=medium

  * New upstream release.
  * Rebase patch queue onto this release.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Tue, 30 Jun 2020 09:19:27 -0400

olivetti-mode (1.10.0-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright file to reflect upstream changes.
  * Update my copyright date range.
  * Add upstream contact.
  * Drop emacs25 from Enhances (not useful post buster).
  * Switch to debhelper-compat 13.
  * Add 0001-Restore-established-keybindings.patch; see patch header for
    explanation.
  * Add README.Debian to document this digression from upstream, and
    additionally to provide a solution for users who wish to combine Olivetti
    with org-mode.
  * Add Enhances: elpa-markdown-mode.
  * Rewrite long description, and drop mention of the mode-line hiding
    feature that was dropped in upstream v1.7.1.
  * control: Add "Rules-Requires-Root: no"
  * Declare Standards-Version 4.5.0 (no additional changes required).

 -- Nicholas D Steeves <nsteeves@gmail.com>  Mon, 15 Jun 2020 14:11:05 -0400

olivetti-mode (1.6.1-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Mon, 26 Aug 2019 12:15:49 -0300

olivetti-mode (1.6.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop 0001-Fix-version-declared-in-ELPA-header.patch
    - Not needed for this release.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Wed, 06 Jun 2018 17:02:40 -0400

olivetti-mode (1.6.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Add missing homepage.
    - Change section to editors, which is more accurate and discoverable.
    - Relax debhelper dep to allow backporting.
    - Switch Vcs from alioth to salsa.
    - Update Maintainer team name and email address.
  * debian/copyright: Add 2017 to copyright range.
  * Update 0001-Fix-version-declared-in-ELPA-header.patch to declare 1.6.0.
  * Declare Standards-Version: 4.1.4. (No additional changes needed)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Sat, 26 May 2018 21:36:45 -0400

olivetti-mode (1.5.9-1) unstable; urgency=medium

  * Initial release. (Closes: #879877)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Tue, 09 Jan 2018 13:41:42 -0500
