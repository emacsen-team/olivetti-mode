Source: olivetti-mode
Section: editors
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Nicholas D Steeves <sten@debian.org>
Build-Depends: debhelper-compat (= 13)
             , dh-elpa
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/emacsen-team/olivetti-mode
Vcs-Git: https://salsa.debian.org/emacsen-team/olivetti-mode.git
Homepage: https://github.com/rnkn/olivetti

Package: elpa-olivetti
Architecture: all
Depends: ${elpa:Depends}
       , ${misc:Depends}
Recommends: emacs
Enhances: emacs
        , elpa-markdown-mode
Description: Emacs minor mode to more comfortably read and write long-lined prose
 Olivetti, a minor mode for Emacs, displays content soft-wrapped to a
 comfortable width, centered inside the larger window.  The visual effect of
 this is similar to the sheet of paper in WYSIWYG word-processors such as
 LibreOffice, and is similar to distraction-limiting software such as
 FocusWriter, WriteRoom, and various Markdown editors.  Olivetti-mode is
 usually used for writing prose and long documents, but when used for reading
 it may slightly increase one's reading speed and one's ability to recall the
 material.
 .
 Features:
   * Exclusively affects buffers for which it has been activated.
   * Dynamically manages window margins so that text remains centered.
   * Interactive modification of `text-body-width` at any time.
   * When `olivetti-body-width` is set to an integer, the text area will
     scale with the size of the font; whereas, when a fraction or float
     is used, the apparent size of the text area will remain visually
     proportioned to the size of the window.
   * Optionally remember the state of visual-line-mode on entry and recall
     its state on exit.  Olivetti enables visual-line-mode by default.
